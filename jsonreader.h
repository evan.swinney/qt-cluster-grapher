#ifndef JSONREADER_H
#define JSONREADER_H

/* HTTP stuff */
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

/* Basics */
#include <QString>
#include <QByteArray>
#include <QTimer>
#include <QUrl>
#include <QEventLoop>

/* JSON Stuff */
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

class JSONReader: public QObject {
    Q_OBJECT
public:
    JSONReader(QUrl url, QObject* parent = nullptr);

private:
    QNetworkAccessManager* manager;
    QUrl url;
    QTimer* queryTimer;

signals:
    void newDataAvailable(QJsonObject object);

private slots:
    void queryTimeout();
    void requestFinished(QNetworkReply* reply);
};

#endif // JSONREADER_H
