#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);

    QGridLayout* grid = new QGridLayout();
    QWidget* centralWidget = new QWidget(this);
    centralWidget->setLayout(grid);

    for(int i = 0; i < 2; i++)
    {
        for (int j = 0; j < 2; j++)
        {
            charts.append(new Chart);
//            charts.back()->setTitle("Dynamic spline chart");
//            charts.back()->addDataSeries("series1");
//            charts.back()->addDataSeries("series2");
//            charts.back()->addDataSeries("series3");
//            charts.back()->startTimer();

            chartViews.append(new QChartView(charts.back()));
            chartViews.back()->setRenderHint(QPainter::Antialiasing);

            grid->addWidget(chartViews.back(), i, j);
        }
    }

    charts.at(0)->setTitle("cpuTemperature");
    charts.at(1)->setTitle("ambientTemp");
    charts.at(2)->setTitle("totalCpuUtilization");
    charts.at(3)->setTitle("moduleCurrentDraw");

    this->setCentralWidget(centralWidget);
//    window.resize(1000, 1000);
    this->setWindowState(Qt::WindowMaximized);

    jsonReader = new JSONReader(QUrl("http://192.168.1.11:8081/api/getAllHostData"), this);
    connect(jsonReader, &JSONReader::newDataAvailable, this, &MainWindow::populateGraphs);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::populateGraphs(QJsonObject object) {
//    qDebug() << object;

    qreal currentTime = (qreal)QDateTime::currentSecsSinceEpoch();
//    qDebug() << object;
//    QJsonObject pi01 = object["pi01"].toObject();
//    qreal cpuTemperature = pi01["cpuTemperature"].toDouble();
//    qreal ambientTemperature = pi01["ambientTemp"].toDouble();
//    qreal backplaneVoltage = pi01["backplaneVoltage"].toDouble();
////    QString receivedTime = pi01["recTime"];

//    charts.at(0)->updateSeries("series1", currentTime, cpuTemperature);
//    charts.at(0)->updateSeries("series2", currentTime, ambientTemperature);
//    charts.at(1)->updateSeries("series1", currentTime, backplaneVoltage);

    for(const QString& hostname: object.keys())
    {
        for(Chart* chart: charts)
            chart->addDataSeries(hostname);

        QJsonObject aPi = object[hostname].toObject();

        charts.at(0)->updateSeries(hostname, currentTime, aPi["cpuTemperature"].toDouble());

        if(hostname != "pi00")
            charts.at(1)->updateSeries(hostname, currentTime, aPi["ambientTemp"].toDouble());

        charts.at(2)->updateSeries(hostname, currentTime, aPi["totalCpuUtilization"].toDouble());

        if(hostname != "pi00")
            charts.at(3)->updateSeries(hostname, currentTime, aPi["moduleCurrentDraw"].toDouble());
    }
}
