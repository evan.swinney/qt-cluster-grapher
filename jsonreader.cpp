#include "jsonreader.h"

JSONReader::JSONReader(QUrl url, QObject* parent): QObject(parent) {
    this->url = url;
    manager = new QNetworkAccessManager(this);
    connect(manager, &QNetworkAccessManager::finished, this, &JSONReader::requestFinished);

    queryTimer = new QTimer(this);
    connect(queryTimer, &QTimer::timeout, this, &JSONReader::queryTimeout);
    queryTimer->start(2000);
}

void JSONReader::queryTimeout() {
    manager->get(QNetworkRequest(url));
}

void JSONReader::requestFinished(QNetworkReply* reply) {
    if(reply->error() != QNetworkReply::NoError) {
        qDebug() << reply->errorString();
        return;
    }

    QJsonParseError parseError;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(reply->readAll(), &parseError);

    if(parseError.error != QJsonParseError::NoError) {
        qDebug() << parseError.errorString();
        return;
    }

    emit newDataAvailable(jsonDoc.object());
}
