#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/* GUI stuff */
#include <QMainWindow>
#include <QGridLayout>
#include <QPushButton>

/* Chart stuff */
#include <QtCharts/QChartView>
#include <QtCharts/QSplineSeries>

/* Basic stuff */
#include <QString>
#include <QList>
#include <QPointF>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QTimer>

/* JSON Stuff */
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

/* Custom classes */
#include "chart.h"
#include "jsonreader.h"

QT_CHARTS_USE_NAMESPACE

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    JSONReader* jsonReader;

    QList<Chart*> charts;
    QList<QChartView*> chartViews;

    QNetworkAccessManager* networkAccessManager;
    QTimer* requestTimer;

    QPushButton* button;

    QChart* cpuUtilChart;
    QChart* ramUtilChart;
    QChart* cpuTempChart;
    QChart* networkUtilChart;

    QMap<QString, QSplineSeries*> cpuUtilSeries;
    QMap<QString, QSplineSeries*> ramUtilSeries;
    QMap<QString, QSplineSeries*> cpuTempSeries;
    QMap<QString, QSplineSeries*> networkUtilSeries;

    void setupCpuUtilChart();
    void setupRamUtilChart();
    void setupCpuTempChart();
    void setupNetworkUtilChart();

private slots:
    void populateGraphs(QJsonObject object);
};

#endif // MAINWINDOW_H
