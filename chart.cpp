/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Charts module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "chart.h"

Chart::Chart(QGraphicsItem *parent, Qt::WindowFlags wFlags):
    QChart(QChart::ChartTypeCartesian, parent, wFlags)
{
    m_axisX = new QValueAxis();
    m_axisY = new QValueAxis();
    addAxis(m_axisX, Qt::AlignBottom);
    addAxis(m_axisY, Qt::AlignLeft);
    m_axisX->setTickCount(5);
    m_axisY->setRange(0, 10);

    QObject::connect(&m_timer, &QTimer::timeout, this, &Chart::handleTimeout);
    m_timer.setInterval(1000);

    //legend()->hide();
    setAnimationOptions(QChart::SeriesAnimations);
    setAnimationDuration(500);

    seriesMap = new QMap<QString, QSplineSeries*>();
}

Chart::~Chart() {

}

void Chart::startTimer() {
    m_timer.start();
}

void Chart::addDataSeries(QString seriesName) {
    if(seriesMap->contains(seriesName))
        return;

    QSplineSeries *series = new QSplineSeries(this);
    series->setName(seriesName);
    seriesMap->insert(seriesName, series);
    addSeries(series);
    series->attachAxis(m_axisX);
    series->attachAxis(m_axisY);
}

void Chart::deleteDataSeries(QString seriesName) {

}

void Chart::updateSeries(QString seriesName, qreal Xvalue, qreal Yvalue) {
//    qDebug() << "Updating series " << seriesName << " with value " << Xvalue << " , " << Yvalue;
    QSplineSeries* series = seriesMap->value(seriesName);

    if(series->pointsVector().size() > 2000)
        series->removePoints(0, 1800);

    series->append(Xvalue, Yvalue);

    m_axisX->setRange(Xvalue-50.0, Xvalue+5.0);
    UpdateRanges();
}

//Gets the total range for all the series and scales the axes accordingly
void Chart::UpdateRanges() {

    qreal minY = std::numeric_limits<qreal>::max();
    qreal maxY = std::numeric_limits<qreal>::min();
    qreal minX = std::numeric_limits<qreal>::max();
    qreal maxX = std::numeric_limits<qreal>::min();

    for(auto series : seriesMap->values()) {
        if(series->points().isEmpty())
            continue;

        for(const QPointF& p : series->pointsVector()) {
            minX = qMin(minX, p.x());
            maxX = qMax(maxX, p.x());
            minY = qMin(minY, p.y());
            maxY = qMax(maxY, p.y());
        }
    }
    m_axisY->setRange(minY-minY*0.05, maxY+maxY*0.05);
}

void Chart::handleTimeout() {
    time += 1.0;
    for(auto key : seriesMap->keys()){

        updateSeries(key,time,QRandomGenerator::global()->bounded(5.0)-2.5);
    }
//    m_axisX->setRange(time-10, time+10);
}
